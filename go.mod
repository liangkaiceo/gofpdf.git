module gitee.com/liangkaiceo/gofpdf

go 1.16

require (
	gitee.com/liangkaiceo/gofpdi v1.0.14
	github.com/boombuler/barcode v1.0.1
	github.com/ruudk/golang-pdf417 v0.0.0-20201230142125-a7e3863a1245
	golang.org/x/image v0.0.0-20210504121937-7319ad40d33e
)
